<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 9/21/2016
 * Time: 00:07
 */

namespace mindgeek;

require_once ("Logg.php");

require_once ("ICms.php");
class CmsB implements ICms
{
    protected $arr;
    public function __construct($args){
        $this->arr = json_decode(json_encode((array)simplexml_load_string($args)),1);
    }
    public function methodThatCalculatesTheAverage( ){
        try {
            $output = "";

            $output .= "The id is:  " . $this->arr['id'] . "<br>";

            $output .= "The name is:  " . $this->arr['firstname'] . "<br>";


            foreach ($this->arr as $key => $value) {

                if ($key == 'grades') {

                    $res = $value;

                    $output .= "The grades are:  " . implode(",", $res) . "<br>";

                    //sort array
                    sort($res);

                    //delete first element
                    array_shift($res);

                    $sum = array_sum($res);
                    $output .= "The average is:  " . $sum . "<br>";

                    if ($sum >= 7) {
                        $output .= "Pass" . "<br>";
                        return $output;
                    } else {

                        $output .= "Fail" . "<br>";
                        return $output;
                    }
                }
            }
        }catch (\Exception $e){
            $logger = Logg::getInstance();
            $logger->log($e->getMessage().", Line:".$e->getLine().", File:".$e->getFile()."<br>");
            echo ($logger->get_logs());die; // Print out all logs

        }

    }
    public function methodThatTransferTheStudentResult($file){
        echo $file;
    }

}