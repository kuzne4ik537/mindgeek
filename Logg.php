<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 9/21/2016
 * Time: 02:03
 */

namespace mindgeek;


class Logg
{
    private static $instance = NULL;
    private $logs;

    private function __construct() {
       // $this->logs = array();
    }

    public static  function getInstance() {
        // Instantiate itself if not instantiated
        if(static::$instance === NULL) {
            static::$instance = new Logg();
        }
        return static::$instance;
    }

    public function log($message) {
        if(!empty($message)) {
            $this->logs .= $message."<br>";
        }
    }

    public function get_logs() {
        return $this->logs;
    }
}