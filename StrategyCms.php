<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 9/21/2016
 * Time: 00:12
 */

namespace mindgeek;

require_once ("Logg.php");
require_once ("Cms.php");
require_once ("CmsB.php");
class StrategyCms
{

    protected $args;
    protected $cms;
    public function __construct($array,$id){
        try {
            foreach ($array as $val) {

                if ($val['schollBoard'] === "BestSchool" && $val['id'] == $id) {
                    $json = json_encode($val, JSON_PRETTY_PRINT);
                    $this->args = $json;
                    $this->cms = new Cms($this->args);
                    break;
                }
                if ($val['schollBoard'] === "WorseSchool" && $val['id'] == $id) {

                    if(!class_exists("mindgeek/SimpleXMLElement")){
                        throw new \Exception("File SimpleXml doesn't exists");
                    }
                    $xml_data = new  SimpleXMLElement('<?xml version="1.0"?><data></data>');
                    $this->array_to_xml($val, $xml_data);
                    $result = $xml_data->asXML('/file/path/name.xml');

                    $this->args = $result;
                    $this->cms = new CmsB($this->args);
                    break;
                }

            }
        }catch (\Exception $e){
            $logger = Logg::getInstance();
            $logger->log($e->getMessage().", Line:".$e->getLine().", File:".$e->getFile()."<br>");
            echo ($logger->get_logs());die; // Print out all logs

        }
    }
    public function  output(){
        $file = $this->cms-> methodThatCalculatesTheAverage();
        $this->cms-> methodThatTransferTheStudentResult($file);
    }
    //array to xml
    function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ){
                    $key = 'item'.$key; //dealing with <0/>..<n/> issues
                }
                $subnode = $xml_data->addChild($key);
                array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }
}

$students = [
    [
        'id' => '1',
        'schollBoard' => 'BestSchool',
        'firstname' => 'Dimitri',
        'grades' => Array (8,6,10,4)
    ],

    [
        'id' => '2',
        'schollBoard' => 'WorseSchool',
        'firstname' => 'Ievgen',
        'grades' => Array (2,3)
    ],

    [
        'id' => '3',
        'schollBoard' => 'BestSchool',
        'firstname' => 'Bob',
        'grades' => Array (10,2,11)
    ]
];
$mama = new StrategyCms($students,3);
$mama->output();