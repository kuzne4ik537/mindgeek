<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 9/21/2016
 * Time: 00:03
 */

namespace mindgeek;



interface ICms
{
    public function methodThatCalculatesTheAverage( );
    public function methodThatTransferTheStudentResult($file);
}